#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(const uint32_t width, const uint32_t height);

void free_image(struct image img);

struct pixel get_pixel(const struct image img, const uint32_t x, const uint32_t y);

void set_pixel(struct image *img, const uint32_t x, const uint32_t y, const struct pixel p);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
