#include "bmp.h"
#include "image.h"

#include <stdint.h>
#include <stdio.h>


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

int get_padding(uint64_t width) {
    return (int) (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status is_header_valid(struct bmp_header header) {
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header.biCompression != 0) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);

    if(is_header_valid(header) != READ_OK) {
        return is_header_valid(header);
    }

    fseek(in, header.bOffBits, SEEK_SET);

    const int padding = get_padding(header.biWidth);
    struct image temp = create_image(header.biWidth, header.biHeight);

    for (uint64_t i = 0; i < header.biHeight; i++) {
        for (uint64_t j = 0; j < header.biWidth; j++) {
            if(fread(&temp.data[i * header.biWidth + j], sizeof(struct pixel), 1, in) != 1) {
                free_image(temp);
                return READ_INVALID_HEADER;
            }
        }

        if (padding) {
            if(fseek(in, padding, SEEK_CUR) != 0) {
                free_image(temp);
                return READ_INVALID_HEADER;
            }
        }
    }

    *img = temp;
    return READ_OK;
}

struct bmp_header generate_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = width * height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum write_status to_bmp(FILE *out, struct image const img) {
    struct bmp_header header = generate_header(img.width, img.height);

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    const int padding = get_padding(img.width);
    for (uint64_t i = 0; i < img.height; i++) {
        for (uint64_t j = 0; j < img.width; j++) {
            fwrite(&img.data[i * img.width + j], sizeof(struct pixel), 1, out);
        }
        if (padding) {
            fwrite(img.data, padding, 1, out);
        }
    }
    return WRITE_OK;
}
