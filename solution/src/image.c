#include "image.h"

#include <stdint.h>
#include <stdlib.h>

struct image create_image(const uint32_t width, const uint32_t height) {
    struct image img;

    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));

    return img;
}

void free_image(struct image img) {
    free(img.data);
}

struct pixel get_pixel(const struct image img, const uint32_t x, const uint32_t y) {
    return img.data[y * img.width + x];
}

void set_pixel(struct image *img, const uint32_t x, const uint32_t y, const struct pixel p) {
    img->data[y * img->width + x] = p;
}
