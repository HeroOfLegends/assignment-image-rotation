#include "transformations.h"

#include "image.h"

#include <stdint.h>

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const source) {
    const uint64_t WIDTH = source.height;
    const uint64_t HEIGHT = source.width;
    struct image img = create_image(WIDTH, HEIGHT);

    for (uint64_t i = 0; i < WIDTH; i++) {
        for (uint64_t j = 0; j < HEIGHT; j++) {
            struct pixel p = get_pixel(source, j, i);
            set_pixel(&img, WIDTH - i - 1, j, p);
        }
    }

    return img;
}
