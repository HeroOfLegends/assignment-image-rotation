#include "image.h"
#include "bmp.h"
#include "transformations.h"

#include <stdbool.h>
#include <stdio.h>



bool read_image(const char *const filename, struct image *img) {
    FILE *in = fopen(filename, "rb");
    if (in == NULL) {
        false;
    }

    enum read_status status = from_bmp(in, img);
    fclose(in);
    if (status != READ_OK) {
        false;
    }

    return true;
}

bool write_image(const char *const filename, const struct image img) {
    FILE *out = fopen(filename, "wb");
    if (out == NULL) {
        return false;
    }

    enum write_status status = to_bmp(out, img);
    fclose(out);

    if (status != WRITE_OK) {
        return false;
    }

    return true;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image>", argv[0]);
        return 1;
    }

    struct image img;
    if (!read_image(argv[1], &img)) {
        fprintf(stderr, "Error reading image");
        return 1;
    }

    struct image rotated = rotate(img);
    free_image(img);

    if (!write_image(argv[2], rotated)) {
        fprintf(stderr, "Error writing image");
        return 1;
    }

    free_image(rotated);

    return 0;
}
